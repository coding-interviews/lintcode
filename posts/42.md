# 42. 最大子数组 II / Maximum Subarray II

回顾一下最大子数组的做法，我们设$`f(i)`$为$`x_i`$为结尾的最大连续子序列和，对于下一个数字，它可以选择与前面的子数组拼接，也可以选择不拼接，$`f(i)`$为这两种情况中较大的一种，即：

```math
f(i) = \max\{f(i-1) + x_i, x_i\}
```

这道题换成了两个子数组，且这两个子数组不能相互重叠。那么，我们可以遍历每个“分割线”，分别找到分割线左边和右边最大的子数组，两者相加即可。设$`f(i)`$表示以$`x_i`$结尾的最大子数组的和，$`g(i)`$表示以$`x_i`$开头的最大子数组的和，那么最终结果应当为：

```math
\max_{0\le i\le N-2}\left\{\max_{0\le j \le i}f(j) + \max_{i+1\le j \le N-1}g(j)\right\}
```

在具体实现上，对于每个$`i`$，我们不需要把左边的$`f(i)`$和右边的$`g(i)`$全部遍历一遍，而只需要像前缀和一样，将他们累积在一起，这样就可以将时间复杂度降低至$`O(N)`$。

```math
\tilde{f}(i) = \max\left\{\tilde{f}(i-1), f(i)\right\}
```

```cpp
class Solution
{
public:
    /*
     * @param nums: A list of integers
     * @return: An integer denotes the sum of max two non-overlapping subarrays
     */
    int maxTwoSubArrays(vector<int> &nums)
    {
        vector<int> dp_left(nums.size()), dp_right(nums.size());
        dp_left.front() = nums.front(), dp_right.back() = nums.back();

        for(int i = 1; i < nums.size(); i++)
            dp_left[i] = max(dp_left[i - 1] + nums[i], nums[i]);
        for(int i = nums.size() - 2; i >= 0; i--)
            dp_right[i] = max(dp_right[i + 1] + nums[i], nums[i]);

        for(int i = 1; i < nums.size(); i++)
            dp_left[i] = max(dp_left[i], dp_left[i - 1]);

        int result = INT_MIN;
        for(int i = nums.size() - 2; i >= 0; i--)
        {
            dp_right[i] = max(dp_right[i], dp_right[i + 1]);
            result = max(result, dp_left[i] + dp_right[i + 1]);
        }
        return result;
    }
};
```
